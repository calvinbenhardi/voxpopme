/**
 * VOXPOPME
 * RN Streaming
 */

import React, { Component } from 'react';
import { AppRegistry } from 'react-native';
import App from './src/App'

AppRegistry.registerComponent('voxpopme', () => App);
