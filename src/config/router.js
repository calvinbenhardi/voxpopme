import { StackNavigator } from 'react-navigation';

import Login from './../modules/Login';
import Task from './../modules/Task';
import Video from './../modules/Video';
import Upload from './../modules/Upload';


const AppStack = StackNavigator({
    Login: {
      screen: Login,
      header: { 
        visible:false 
      },
    },
    Task: {
      screen: Task,
    },
    Video: {
    	screen: Video,
    },
    Upload: {
    	screen: Upload,
    },
  },
  { headerMode: 'none' },
);

export default AppStack