import React, { Component } from 'react';
import { WebView } from 'react-native';
import WebViewAndroid from 'react-native-webview-android';


export default class Upload extends Component {
	render() { 
		const {state} = this.props.navigation;
		const jsCode="document.querySelector('.footer-tiny').style.visibility = 'hidden';"
	    return (
	      <WebViewAndroid
	          ref="webViewAndroidSample"
	          javaScriptEnabled={true}
	          injectedJavaScript={jsCode}
	          url="https://driveuploader.com/upload/nICEyuMJna/"
	          style={{marginTop: 20, flex: 1}} />
	    );
	}
}