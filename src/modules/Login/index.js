import React, { Component } from 'react';
import { navigate } from 'react-navigation';
import {View, Image, StyleSheet, AsyncStorage } from 'react-native';
import { Container, Content, Form, Item, Input, Button, Text, Label } from 'native-base';

import Config from '../../config/config.js';
import Task from '../Task';

export default class Login extends Component {

  constructor(props){
    super(props);
    this.state = {
      username:'',
      password:'',
      token:'',
    }
  }

  login(){
    fetch(Config.API + '/api-token-auth/',{
      method: 'POST',
      headers:{
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({username:this.state.username, password:this.state.password})
    },
    )
    .then(response => response.json())
    .then(json => {
      const data = json;  
      AsyncStorage.setItem('token', data.token);
      this.goTo(data.token)
    })
    .catch(function(error){
      alert("Fetching failed");
    })
  }

  goTo(data){
    const {navigate} = this.props.navigation;
    if(data.token != null || data.token != ''){
      navigate('Task',{"token":data})
    }else{
      navigate('Login',{"token":null})
    }
  }
  
  render() {
    return (
      <Container style={{backgroundColor:'white',}}>
        <View style={{marginTop: 128, justifyContent: 'center',
                alignItems: 'center',}
              }>
          <Image source={require('../../static/img/logo.jpg')} style={styles.image}/>
        </View>
        <Content>
          <Form>
            <Item floatingLabel>
              <Label>Username</Label>
              <Input onChangeText={(value) => this.setState({username: value})}/>
            </Item>
            <Item floatingLabel>
              <Label>Password</Label>
              <Input
                secureTextEntry={true}
                onChangeText={(value) => this.setState({password: value})}/>
            </Item>
          </Form>
          <Button block
              style={{ backgroundColor:'#b8303b', marginTop:30, marginLeft:10, marginRight:10,}}
              onPress={this.login.bind(this)}>
                <Text> Login </Text>
          </Button>
        </Content>
      </Container>
    );
  }
}

var styles = StyleSheet.create({
  image: {
    width:300,
    height: 75,
    alignItems : "center",
    justifyContent: 'center',
    resizeMode: "contain",             //      "flex-end" or "space-between" here
  },
});