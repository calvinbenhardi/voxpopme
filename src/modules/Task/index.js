import React, { Component } from 'react';
import {AsyncStorage } from 'react-native'
import { navigate } from 'react-navigation';

import { Container, Content, Card, CardItem, Text, Body, Button, H2 } from 'native-base';

import Data from './data.json'
import Config from '../../config/config.js'


export default class Task extends Component {
  constructor(props){
    super(props);
    this.state = {
      task:[],
      token:'',
    }
  }

  componentWillMount(){
    AsyncStorage.getItem('token').then((value) => this.setState({ 'token': value }))
  }

  componentDidMount(){
    const {state} = this.props.navigation;
    var myInit = {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + state.params.token,
      }
    }

    fetch(Config.API + '/api/task', myInit)
      .then((response) => response.json())
      .then((data) => {
        this.setState({task: data});
      })
      .catch((error) => {
        alert(error)
        this.setState({error: error});
      });
  }
  
  render() {
    let task = this.state.task;
    const { navigate } = this.props.navigation;
    let card = task.map(function(val,key){
      let branchType = '';
      if(val.branch.branch_type === 'ATM'){
        branchType = val.branch.address;
      }else{
        branchType = null;
      }
      return(
          <Card style={{flex: 0, marginLeft:10, marginRight:10, borderRadius:6, borderWidth: 1, backgroundColor:'#F4F6F6' }} key={key}>
            <CardItem>
                <Body>
                  <Text style={{ fontSize:19, fontWeight:'bold' }}>{val.scenario.name}</Text>
                  <Text>
                    {val.scenario.description}
                  </Text>
                </Body>
            </CardItem>
            <CardItem>
              <Body>
                <Text style={{ fontSize:19, fontWeight:'bold' }}>Bank Information</Text>
                <Text>{val.branch.bank_name}</Text>
                <Text>{val.branch.code}</Text>
                <Text>{branchType}</Text>
              </Body>
            </CardItem>
            <CardItem>
              <Button 
                textStyle={{color: '#b8303b'}}
                style={{ backgroundColor:'#b8303b', flexDirection:'row', alignItems:'center'}}
                onPress={() => navigate('Video', {})}>
                <Text>Start</Text>
              </Button>
                <Button 
                  style={{ marginLeft:20 }}
                  onPress={() => navigate('Upload', {"link": val.video_upload_link})}>
                  <Text>Upload</Text>
                </Button>
            </CardItem>
          </Card>
      )
    })


    return (
      <Container style={{marginTop: 50}}>
        <Content>
          <H2 style={{ textAlign: 'center', marginBottom:50}}>My Task</H2>
          {card}
        </Content>
      </Container>
    );
  }
}


