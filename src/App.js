import React, { Component } from 'react';
import { AppRegistry } from 'react-native';

import AppStack  from './config/router';


export default class App extends Component {
  render() {
    return (
      <AppStack/>
    );
  }
}